CellProfiler Pipeline: http://www.cellprofiler.org
Version:1
SVNRevision:11047

LoadImages:[module_num:1|svn_version:\'11031\'|variable_revision_number:11|show_window:False|notes:\x5B\x5D]
    File type to be loaded:individual images
    File selection method:Text-Regular expressions
    Number of images in each group?:3
    Type the text that the excluded images have in common:Do not use
    Analyze all subfolders within the selected folder?:All
    Input image file location:Elsewhere...\x7C/g/almfscreen/mitra/output
    Check image sets for missing or duplicate files?:Yes
    Group images by metadata?:No
    Exclude certain files?:No
    Specify metadata fields to group by:
    Select subfolders to analyze:Jansen_Mitra_CENPA_LT05_batch1_20,Jansen_Mitra_CENPA_LT01_batch1_16,Jansen_Mitra_CENPA_LT06_batch1_14,Jansen_Mitra_CENPA_LT04_batch1_23,Jansen_Mitra_CENPA_LT03_batch1_24,Jansen_Mitra_CENPA_LT02_batch1_10,Jansen_Mitra_CENPA_LT07_batch1_03
    Image count:3
    Text that these images have in common (case-sensitive):.*C01.ome.tif
    Position of this image in each group:1
    Extract metadata from where?:Both
    Regular expression that finds metadata in the file name:^(?P<ImageName>.*)--T000.*
    Type the regular expression that finds metadata in the subfolder path:(?P<Root>.*)\x5B\\\\/\x5Doutput\x5B\\\\/\x5D(?P<Plate>.*)\x5B\\\\/\x5D(?P<Well>.*)\x5B\\\\/\x5D(?P<Position>.*)$
    Channel count:1
    Group the movie frames?:No
    Grouping method:Interleaved
    Number of channels per group:3
    Load the input as images or objects?:Images
    Name this loaded image:Nucleus
    Name this loaded object:Nuclei
    Retain outlines of loaded objects?:No
    Name the outline image:LoadedImageOutlines
    Channel number:1
    Rescale intensities?:Yes
    Text that these images have in common (case-sensitive):.*C02.ome.tif
    Position of this image in each group:2
    Extract metadata from where?:File name
    Regular expression that finds metadata in the file name:.*--(?P<SiRNA>.*)--(?P<Gene>.*)--W(?P<WellNum>.*)--P(?P<PositionNum>.*)--T.*
    Type the regular expression that finds metadata in the subfolder path:.*\x5B\\\\/\x5D(?P<Date>.*)\x5B\\\\/\x5D(?P<Run>.*)$
    Channel count:1
    Group the movie frames?:No
    Grouping method:Interleaved
    Number of channels per group:3
    Load the input as images or objects?:Images
    Name this loaded image:Old
    Name this loaded object:Nuclei
    Retain outlines of loaded objects?:No
    Name the outline image:LoadedImageOutlines
    Channel number:1
    Rescale intensities?:Yes
    Text that these images have in common (case-sensitive):.*C03.ome.tif
    Position of this image in each group:3
    Extract metadata from where?:None
    Regular expression that finds metadata in the file name:^(?P<Plate>.*)_(?P<Well>\x5BA-P\x5D\x5B0-9\x5D{2})_s(?P<Site>\x5B0-9\x5D)
    Type the regular expression that finds metadata in the subfolder path:.*\x5B\\\\/\x5D(?P<Date>.*)\x5B\\\\/\x5D(?P<Run>.*)$
    Channel count:1
    Group the movie frames?:No
    Grouping method:Interleaved
    Number of channels per group:3
    Load the input as images or objects?:Images
    Name this loaded image:New
    Name this loaded object:Nuclei
    Retain outlines of loaded objects?:No
    Name the outline image:LoadedImageOutlines
    Channel number:1
    Rescale intensities?:Yes

IdentifyPrimaryObjects:[module_num:2|svn_version:\'11047\'|variable_revision_number:8|show_window:False|notes:\x5B\x5D]
    Select the input image:Nucleus
    Name the primary objects to be identified:Nuclei
    Typical diameter of objects, in pixel units (Min,Max):20,60
    Discard objects outside the diameter range?:Yes
    Try to merge too small objects with nearby larger objects?:No
    Discard objects touching the border of the image?:Yes
    Select the thresholding method:Otsu Global
    Threshold correction factor:1
    Lower and upper bounds on threshold:0.0035,1.0
    Approximate fraction of image covered by objects?:0.01
    Method to distinguish clumped objects:Shape
    Method to draw dividing lines between clumped objects:Intensity
    Size of smoothing filter:10
    Suppress local maxima that are closer than this minimum allowed distance:7
    Speed up by using lower-resolution image to find local maxima?:Yes
    Name the outline image:PrimaryOutlines
    Fill holes in identified objects?:Yes
    Automatically calculate size of smoothing filter?:Yes
    Automatically calculate minimum allowed distance between local maxima?:Yes
    Manual threshold:0.0
    Select binary image:None
    Retain outlines of the identified objects?:No
    Automatically calculate the threshold using the Otsu method?:Yes
    Enter Laplacian of Gaussian threshold:0.5
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground
    Automatically calculate the size of objects for the Laplacian of Gaussian filter?:Yes
    Enter LoG filter diameter:5
    Handling of objects if excessive number of objects identified:Continue
    Maximum number of objects:500
    Select the measurement to threshold with:None

IdentifyPrimaryObjects:[module_num:3|svn_version:\'11047\'|variable_revision_number:8|show_window:False|notes:\x5B\'For image background measurements\'\x5D]
    Select the input image:Nucleus
    Name the primary objects to be identified:AllNuclei
    Typical diameter of objects, in pixel units (Min,Max):10,50
    Discard objects outside the diameter range?:No
    Try to merge too small objects with nearby larger objects?:No
    Discard objects touching the border of the image?:No
    Select the thresholding method:Otsu Global
    Threshold correction factor:1
    Lower and upper bounds on threshold:0.0035,1.0
    Approximate fraction of image covered by objects?:0.01
    Method to distinguish clumped objects:None
    Method to draw dividing lines between clumped objects:Intensity
    Size of smoothing filter:10
    Suppress local maxima that are closer than this minimum allowed distance:7
    Speed up by using lower-resolution image to find local maxima?:Yes
    Name the outline image:PrimaryOutlines
    Fill holes in identified objects?:Yes
    Automatically calculate size of smoothing filter?:Yes
    Automatically calculate minimum allowed distance between local maxima?:Yes
    Manual threshold:0.505
    Select binary image:None
    Retain outlines of the identified objects?:No
    Automatically calculate the threshold using the Otsu method?:Yes
    Enter Laplacian of Gaussian threshold:0.5
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground
    Automatically calculate the size of objects for the Laplacian of Gaussian filter?:Yes
    Enter LoG filter diameter:5
    Handling of objects if excessive number of objects identified:Continue
    Maximum number of objects:500
    Select the measurement to threshold with:None

ImageMath:[module_num:4|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Operation:None
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:-0.003
    Set values less than 0 equal to 0?:No
    Set values greater than 1 equal to 1?:No
    Ignore the image masks?:No
    Name the output image:NucleusMinusBg
    Image or measurement?:Image
    Select the first image:Nucleus
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Image
    Select the second image:
    Multiply the second image by:1
    Measurement:

ExpandOrShrinkObjects:[module_num:5|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:Nuclei
    Name the output objects:ShrunkenNuclei
    Select the operation:Shrink objects by a specified number of pixels
    Number of pixels by which to expand or shrink:5
    Fill holes in objects so that all objects shrink to a single point?:No
    Retain the outlines of the identified objects for use later in the pipeline (for example, in SaveImages)?:No
    Name the outline image:ShrunkenNucleiOutlines

MeasureObjectIntensity:[module_num:6|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\'Exclude mitotic/dying cells\'\x5D]
    Hidden:1
    Select an image to measure:NucleusMinusBg
    Select objects to measure:ShrunkenNuclei

CalculateMath:[module_num:7|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Name the output measurement:CV
    Operation:Divide
    Select the numerator measurement type:Object
    Select the numerator objects:ShrunkenNuclei
    Select the numerator measurement:Intensity_StdIntensity_NucleusMinusBg
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Select the denominator measurement type:Object
    Select the denominator objects:ShrunkenNuclei
    Select the denominator measurement:Intensity_MeanIntensity_NucleusMinusBg
    Multiply the above operand by:1
    Raise the power of above operand by:1
    Take log10 of result?:No
    Multiply the result by:1
    Raise the power of result by:1

DisplayDataOnImage:[module_num:8|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Display object or image measurements?:Object
    Select the input objects:ShrunkenNuclei
    Measurement to display:Math_CV
    Select the image on which to display the measurements:Nucleus
    Text color:red
    Name the output image that has the measurements displayed:DisplayImage
    Font size (points):10
    Number of decimals:2
    Image elements to save:Image

FilterObjects:[module_num:9|svn_version:\'11025\'|variable_revision_number:5|show_window:False|notes:\x5B\'Exclude mitotic/dying cells\'\x5D]
    Name the output objects:InterphaseShrunkenNuclei
    Select the object to filter:ShrunkenNuclei
    Filter using classifier rules or measurements?:Measurements
    Select the filtering method:Limits
    Select the objects that contain the filtered objects:None
    Retain outlines of the identified objects?:No
    Name the outline image:InterphaseNuclei
    Rules file location:Default Input Folder\x7CNone
    Rules file name:rules.txt
    Measurement count:1
    Additional object count:0
    Select the measurement to filter by:Math_CV
    Filter using a minimum measurement value?:No
    Minimum value:0
    Filter using a maximum measurement value?:Yes
    Maximum value:0.17

ConvertObjectsToImage:[module_num:10|svn_version:\'11025\'|variable_revision_number:1|show_window:False|notes:\x5B\x5D]
    Select the input objects:InterphaseShrunkenNuclei
    Name the output image:InterphaseShrunkenNuclei
    Select the color type:Binary (black & white)
    Select the colormap:Default

MeasureObjectIntensity:[module_num:11|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Hidden:1
    Select an image to measure:InterphaseShrunkenNuclei
    Select objects to measure:Nuclei

FilterObjects:[module_num:12|svn_version:\'11025\'|variable_revision_number:5|show_window:False|notes:\x5B\x5D]
    Name the output objects:InterphaseNuclei
    Select the object to filter:Nuclei
    Filter using classifier rules or measurements?:Measurements
    Select the filtering method:Limits
    Select the objects that contain the filtered objects:None
    Retain outlines of the identified objects?:Yes
    Name the outline image:InterphaseNuclei
    Rules file location:Default Input Folder\x7CNone
    Rules file name:rules.txt
    Measurement count:1
    Additional object count:0
    Select the measurement to filter by:Intensity_MaxIntensity_InterphaseShrunkenNuclei
    Filter using a minimum measurement value?:Yes
    Minimum value:1
    Filter using a maximum measurement value?:No
    Maximum value:1

MeasureImageQuality:[module_num:13|svn_version:\'11045\'|variable_revision_number:4|show_window:False|notes:\x5B\x5D]
    Calculate metrics for which images?:All loaded images
    Image count:1
    Scale count:1
    Threshold count:1
    Select the images to measure:
    Include the image rescaling value?:Yes
    Calculate blur metrics?:Yes
    Window size for blur measurements:20
    Calculate saturation metrics?:No
    Calculate intensity metrics?:No
    Calculate thresholds?:No
    Use all thresholding methods?:Yes
    Select a thresholding method:Otsu Global
    Typical fraction of the image covered by objects:0.1
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground

MaskImage:[module_num:14|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Select the input image:New
    Name the output image:NewMaskOutsideNuclei
    Use objects or an image as a mask?:Objects
    Select object for mask:AllNuclei
    Select image for mask:None
    Invert the mask?:Yes

MeasureImageIntensity:[module_num:15|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the image to measure:NewMaskOutsideNuclei
    Measure the intensity only from areas enclosed by objects?:No
    Select the input objects:None

ImageMath:[module_num:16|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Operation:Subtract
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0
    Set values less than 0 equal to 0?:No
    Set values greater than 1 equal to 1?:No
    Ignore the image masks?:No
    Name the output image:NewMinusBG
    Image or measurement?:Image
    Select the first image:New
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Measurement
    Select the second image:
    Multiply the second image by:1
    Measurement:Intensity_MedianIntensity_NewMaskOutsideNuclei

MaskImage:[module_num:17|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Select the input image:Old
    Name the output image:OldMaskOutsideNuclei
    Use objects or an image as a mask?:Objects
    Select object for mask:AllNuclei
    Select image for mask:None
    Invert the mask?:Yes

MeasureImageIntensity:[module_num:18|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the image to measure:OldMaskOutsideNuclei
    Measure the intensity only from areas enclosed by objects?:No
    Select the input objects:None

ImageMath:[module_num:19|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Operation:Subtract
    Raise the power of the result by:1
    Multiply the result by:1
    Add to result:0
    Set values less than 0 equal to 0?:No
    Set values greater than 1 equal to 1?:No
    Ignore the image masks?:No
    Name the output image:OldMinusBG
    Image or measurement?:Image
    Select the first image:Old
    Multiply the first image by:1
    Measurement:
    Image or measurement?:Measurement
    Select the second image:
    Multiply the second image by:1
    Measurement:Intensity_MedianIntensity_OldMaskOutsideNuclei

Morph:[module_num:20|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:Old
    Name the output image:OldTophat
    Select the operation to perform:tophat
    Number of times to repeat operation:Once
    Repetition number:2
    Scale:6.0

Crop:[module_num:21|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:OldTophat
    Name the output image:OldTophatNucleusCrop
    Select the cropping shape:Objects
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:0,end
    Top and bottom rectangle positions:0,end
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:No
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:Nuclei

IdentifyPrimaryObjects:[module_num:22|svn_version:\'11047\'|variable_revision_number:8|show_window:False|notes:\x5B\x5D]
    Select the input image:OldTophatNucleusCrop
    Name the primary objects to be identified:OldCentromeres
    Typical diameter of objects, in pixel units (Min,Max):2,40000
    Discard objects outside the diameter range?:No
    Try to merge too small objects with nearby larger objects?:No
    Discard objects touching the border of the image?:Yes
    Select the thresholding method:RobustBackground PerObject
    Threshold correction factor:1
    Lower and upper bounds on threshold:0.000000,1.000000
    Approximate fraction of image covered by objects?:0.01
    Method to distinguish clumped objects:None
    Method to draw dividing lines between clumped objects:Intensity
    Size of smoothing filter:10
    Suppress local maxima that are closer than this minimum allowed distance:7
    Speed up by using lower-resolution image to find local maxima?:Yes
    Name the outline image:OldCentromeres
    Fill holes in identified objects?:No
    Automatically calculate size of smoothing filter?:Yes
    Automatically calculate minimum allowed distance between local maxima?:Yes
    Manual threshold:0.00035
    Select binary image:None
    Retain outlines of the identified objects?:Yes
    Automatically calculate the threshold using the Otsu method?:Yes
    Enter Laplacian of Gaussian threshold:0.5
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground
    Automatically calculate the size of objects for the Laplacian of Gaussian filter?:Yes
    Enter LoG filter diameter:5
    Handling of objects if excessive number of objects identified:Continue
    Maximum number of objects:500
    Select the measurement to threshold with:None

MaskImage:[module_num:23|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Select the input image:OldTophat
    Name the output image:OldTophatCentromereMask
    Use objects or an image as a mask?:Objects
    Select object for mask:OldCentromeres
    Select image for mask:None
    Invert the mask?:No

Morph:[module_num:24|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:New
    Name the output image:NewTophat
    Select the operation to perform:tophat
    Number of times to repeat operation:Once
    Repetition number:2
    Scale:4

Crop:[module_num:25|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:NewTophat
    Name the output image:NewTophatNucleusCrop
    Select the cropping shape:Objects
    Select the cropping method:Coordinates
    Apply which cycle\'s cropping pattern?:Every
    Left and right rectangle positions:0,end
    Top and bottom rectangle positions:0,end
    Coordinates of ellipse center:500,500
    Ellipse radius, X direction:400
    Ellipse radius, Y direction:200
    Use Plate Fix?:No
    Remove empty rows and columns?:No
    Select the masking image:None
    Select the image with a cropping mask:None
    Select the objects:Nuclei

IdentifyPrimaryObjects:[module_num:26|svn_version:\'11047\'|variable_revision_number:8|show_window:False|notes:\x5B\x5D]
    Select the input image:NewTophatNucleusCrop
    Name the primary objects to be identified:NewCentromeres
    Typical diameter of objects, in pixel units (Min,Max):2,40000
    Discard objects outside the diameter range?:No
    Try to merge too small objects with nearby larger objects?:No
    Discard objects touching the border of the image?:Yes
    Select the thresholding method:RobustBackground PerObject
    Threshold correction factor:1.5
    Lower and upper bounds on threshold:0.000000,1.000000
    Approximate fraction of image covered by objects?:0.01
    Method to distinguish clumped objects:None
    Method to draw dividing lines between clumped objects:Intensity
    Size of smoothing filter:10
    Suppress local maxima that are closer than this minimum allowed distance:7
    Speed up by using lower-resolution image to find local maxima?:Yes
    Name the outline image:NewCentromeres
    Fill holes in identified objects?:No
    Automatically calculate size of smoothing filter?:Yes
    Automatically calculate minimum allowed distance between local maxima?:Yes
    Manual threshold:0.0012
    Select binary image:None
    Retain outlines of the identified objects?:Yes
    Automatically calculate the threshold using the Otsu method?:Yes
    Enter Laplacian of Gaussian threshold:0.5
    Two-class or three-class thresholding?:Two classes
    Minimize the weighted variance or the entropy?:Weighted variance
    Assign pixels in the middle intensity class to the foreground or the background?:Foreground
    Automatically calculate the size of objects for the Laplacian of Gaussian filter?:Yes
    Enter LoG filter diameter:5
    Handling of objects if excessive number of objects identified:Continue
    Maximum number of objects:500
    Select the measurement to threshold with:None

MaskImage:[module_num:27|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Select the input image:NewTophat
    Name the output image:NewTophatCentromereMask
    Use objects or an image as a mask?:Objects
    Select object for mask:NewCentromeres
    Select image for mask:None
    Invert the mask?:No

MeasureObjectIntensity:[module_num:28|svn_version:\'11025\'|variable_revision_number:3|show_window:False|notes:\x5B\x5D]
    Hidden:4
    Select an image to measure:OldTophatCentromereMask
    Select an image to measure:OldMinusBG
    Select an image to measure:NewTophatCentromereMask
    Select an image to measure:NewMinusBG
    Select objects to measure:InterphaseNuclei

RescaleIntensity:[module_num:29|svn_version:\'6746\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:Nucleus
    Name the output image:NucleusRS
    Select rescaling method:Stretch each image to use the full intensity range
    How do you want to calculate the minimum intensity?:Custom
    How do you want to calculate the maximum intensity?:Custom
    Enter the lower limit for the intensity range for the input image:0
    Enter the upper limit for the intensity range for the input image:1
    Enter the intensity range for the input image:0.000000,1.000000
    Enter the desired intensity range for the final, rescaled image:0.000000,1.000000
    Select method for rescaling pixels below the lower limit:Mask pixels
    Enter custom value for pixels below lower limit:0
    Select method for rescaling pixels above the upper limit:Mask pixels
    Enter custom value for pixels below upper limit:0
    Select image to match in maximum intensity:None
    Enter the divisor:1
    Select the measurement to use as a divisor:None

RescaleIntensity:[module_num:30|svn_version:\'6746\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:New
    Name the output image:NewRS
    Select rescaling method:Stretch each image to use the full intensity range
    How do you want to calculate the minimum intensity?:Custom
    How do you want to calculate the maximum intensity?:Custom
    Enter the lower limit for the intensity range for the input image:0
    Enter the upper limit for the intensity range for the input image:1
    Enter the intensity range for the input image:0.000000,1.000000
    Enter the desired intensity range for the final, rescaled image:0.000000,1.000000
    Select method for rescaling pixels below the lower limit:Mask pixels
    Enter custom value for pixels below lower limit:0
    Select method for rescaling pixels above the upper limit:Mask pixels
    Enter custom value for pixels below upper limit:0
    Select image to match in maximum intensity:None
    Enter the divisor:1
    Select the measurement to use as a divisor:None

RescaleIntensity:[module_num:31|svn_version:\'6746\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Select the input image:Old
    Name the output image:OldRS
    Select rescaling method:Stretch each image to use the full intensity range
    How do you want to calculate the minimum intensity?:Custom
    How do you want to calculate the maximum intensity?:Custom
    Enter the lower limit for the intensity range for the input image:0
    Enter the upper limit for the intensity range for the input image:1
    Enter the intensity range for the input image:0.000000,1.000000
    Enter the desired intensity range for the final, rescaled image:0.000000,1.000000
    Select method for rescaling pixels below the lower limit:Mask pixels
    Enter custom value for pixels below lower limit:0
    Select method for rescaling pixels above the upper limit:Mask pixels
    Enter custom value for pixels below upper limit:0
    Select image to match in maximum intensity:None
    Enter the divisor:1
    Select the measurement to use as a divisor:None

OverlayOutlines:[module_num:32|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Display outlines on a blank image?:No
    Select image on which to display outlines:NucleusRS
    Name the output image:NucleusOverlay
    Select outline display mode:Color
    Select method to determine brightness of outlines:Max of image
    Width of outlines:1
    Select outlines to display:InterphaseNuclei
    Select outline color:Blue

OverlayOutlines:[module_num:33|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Display outlines on a blank image?:No
    Select image on which to display outlines:NewRS
    Name the output image:NewOverlay
    Select outline display mode:Color
    Select method to determine brightness of outlines:Max of image
    Width of outlines:1
    Select outlines to display:NewCentromeres
    Select outline color:Green

OverlayOutlines:[module_num:34|svn_version:\'11025\'|variable_revision_number:2|show_window:False|notes:\x5B\x5D]
    Display outlines on a blank image?:No
    Select image on which to display outlines:OldRS
    Name the output image:OldOverlay
    Select outline display mode:Color
    Select method to determine brightness of outlines:Max of image
    Width of outlines:1
    Select outlines to display:OldCentromeres
    Select outline color:Red

SaveImages:[module_num:35|svn_version:\'11042\'|variable_revision_number:7|show_window:False|notes:\x5B\x5D]
    Select the type of image to save:Image
    Select the image to save:NucleusOverlay
    Select the objects to save:None
    Select the module display window to save:None
    Select method for constructing file names:Single name
    Select image name for file prefix:None
    Enter single file name:\\g<ImageName>--Nucleus
    Do you want to add a suffix to the image file name?:No
    Text to append to the image name:
    Select file format to use:jpeg
    Output file location:Elsewhere...\x7C\\g<Root>/output--cp/images/\\g<Plate>
    Image bit depth:8
    Overwrite existing files without warning?:Yes
    Select how often to save:Every cycle
    Rescale the images? :Yes
    Save as grayscale or color image?:Grayscale
    Select colormap:gray
    Store file and path information to the saved image?:Yes
    Create subfolders in the output folder?:No

SaveImages:[module_num:36|svn_version:\'11042\'|variable_revision_number:7|show_window:False|notes:\x5B\x5D]
    Select the type of image to save:Image
    Select the image to save:NewOverlay
    Select the objects to save:None
    Select the module display window to save:None
    Select method for constructing file names:Single name
    Select image name for file prefix:None
    Enter single file name:\\g<ImageName>--New
    Do you want to add a suffix to the image file name?:No
    Text to append to the image name:
    Select file format to use:jpeg
    Output file location:Elsewhere...\x7C\\g<Root>/output--cp/images/\\g<Plate>
    Image bit depth:8
    Overwrite existing files without warning?:Yes
    Select how often to save:Every cycle
    Rescale the images? :Yes
    Save as grayscale or color image?:Grayscale
    Select colormap:gray
    Store file and path information to the saved image?:Yes
    Create subfolders in the output folder?:No

SaveImages:[module_num:37|svn_version:\'11042\'|variable_revision_number:7|show_window:False|notes:\x5B\x5D]
    Select the type of image to save:Image
    Select the image to save:OldOverlay
    Select the objects to save:None
    Select the module display window to save:None
    Select method for constructing file names:Single name
    Select image name for file prefix:None
    Enter single file name:\\g<ImageName>--Old
    Do you want to add a suffix to the image file name?:No
    Text to append to the image name:
    Select file format to use:jpeg
    Output file location:Elsewhere...\x7C\\g<Root>/output--cp/images/\\g<Plate>
    Image bit depth:8
    Overwrite existing files without warning?:Yes
    Select how often to save:Every cycle
    Rescale the images? :Yes
    Save as grayscale or color image?:Grayscale
    Select colormap:gray
    Store file and path information to the saved image?:Yes
    Create subfolders in the output folder?:No

ExportToSpreadsheet:[module_num:38|svn_version:\'11042\'|variable_revision_number:7|show_window:False|notes:\x5B\x5D]
    Select or enter the column delimiter:Comma (",")
    Prepend the output file name to the data file names?:No
    Add image metadata columns to your object data file?:No
    Limit output to a size that is allowed in Excel?:No
    Select the columns of measurements to export?:Yes
    Calculate the per-image mean values for object measurements?:Yes
    Calculate the per-image median values for object measurements?:No
    Calculate the per-image standard deviation values for object measurements?:No
    Output file location:Elsewhere...\x7C\\g<Root>/output--cp/tables/\\g<Plate>
    Create a GenePattern GCT file?:No
    Select source of sample row name:Metadata
    Select the image to use as the identifier:None
    Select the metadata to use as the identifier:None
    Export all measurements, using default file names?:No
    Press button to select measurements to export:Image\x7CCount_Nuclei,Image\x7CCount_InterphaseNuclei,Image\x7CImageQuality_PowerLogLogSlope_Old,Image\x7CImageQuality_PowerLogLogSlope_Nucleus,Image\x7CImageQuality_PowerLogLogSlope_New,Image\x7CFileName_Old,Image\x7CFileName_NewOverlay,Image\x7CFileName_NucleusOverlay,Image\x7CFileName_Nucleus,Image\x7CFileName_New,Image\x7CFileName_OldOverlay,Image\x7CIntensity_MedianIntensity_OldMaskOutsideNuclei,Image\x7CIntensity_MedianIntensity_NewMaskOutsideNuclei,Image\x7CPathName_Old,Image\x7CPathName_NewOverlay,Image\x7CPathName_NucleusOverlay,Image\x7CPathName_Nucleus,Image\x7CPathName_New,Image\x7CPathName_OldOverlay,Image\x7CMetadata_PositionNum,Image\x7CMetadata_Plate,Image\x7CMetadata_SiRNA,Image\x7CMetadata_Well,Image\x7CMetadata_WellNum,Image\x7CMetadata_ImageName,Image\x7CMetadata_Position,Image\x7CMetadata_Gene,Image\x7CMetadata_Root,InterphaseNuclei\x7CIntensity_IntegratedIntensity_OldMinusBG,InterphaseNuclei\x7CIntensity_IntegratedIntensity_NewTophatCentromereMask,InterphaseNuclei\x7CIntensity_IntegratedIntensity_NewMinusBG,InterphaseNuclei\x7CIntensity_IntegratedIntensity_OldTophatCentromereMask
    Data to export:Image
    Combine these object measurements with those of the previous object?:No
    File name:\\g<ImageName>--image.csv
    Use the object name for the file name?:No

CreateBatchFiles:[module_num:39|svn_version:\'11025\'|variable_revision_number:4|show_window:False|notes:\x5B\x5D]
    Store batch files in default output folder?:No
    Output folder path:/g/almfscreen/mitra
    Are the cluster computers running Windows?:No
    Hidden\x3A in batch mode:No
    Hidden\x3A default input folder at time of save:/home/mitra
    Hidden\x3A SVN revision number:0
    Local root path:/home/mitra
    Cluster root path:/home/mitra